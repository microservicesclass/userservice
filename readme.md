# User Service

Author: Gustavo Aquino 

The `UserMS` is a FastAPI based microservice designed to manage user CRUD.

## Table of Contents

- [Installation](#installation)


## Installation

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/microservicesclass/userservice.git

2. cd userservice

3. Create an environment
    ```bash
    conda create -n userservice python=3.10

4. conda activate userservice
    
5. Install the requirements:
    ```bash
   pip install -r requirements.txt

6. python main.py

This will start the FastAPI application on http://127.0.0.1:8001/docs.