from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from api import crud, models, database, schemas  # Import schemas
from api.database import SessionLocal
from typing import List 

router = APIRouter()

@router.post("/users/", response_model=schemas.User)  # Use the Pydantic model
def create_user(user: schemas.UserCreate):
    db = SessionLocal()
    db_user = crud.get_user_by_username(db, username=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="Username already registered")
    user = models.User(username=user.username)
    return crud.create_user(db=db, user=user)

@router.get("/users/", response_model=List[schemas.User])  # Use a list of Pydantic models as the response
def list_users(skip: int = 0, limit: int = 10):
    db = SessionLocal()
    users = crud.get_users(db, skip=skip, limit=limit)
    return users

@router.get("/users/id/{user_id}", response_model=schemas.User)  # Use the Pydantic model
def read_user_by_id(user_id: int):
    db = SessionLocal()
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user

@router.get("/users/username/{username}", response_model=schemas.User)  # New route to get user by username
def read_user_by_username(username: str):
    db = SessionLocal()
    db_user = crud.get_user_by_username(db, username=username)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user

@router.put("/users/{user_id}", response_model=schemas.User)  # Use the Pydantic model
def update_user(user_id: int, username: str):
    db = SessionLocal()
    return crud.update_user(db=db, user_id=user_id, new_username=username)

@router.delete("/users/{user_id}", response_model=schemas.User)  # Use the Pydantic model
def delete_user(user_id: int):
    db = SessionLocal()
    db_user = crud.delete_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user
